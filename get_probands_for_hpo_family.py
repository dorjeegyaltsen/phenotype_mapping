import os
import re

import subprocess


def get_larger_file(lis):
    file_sizes = [os.path.getsize(bam) for bam in lis]
    _index = file_sizes.index(max(file_sizes))
    return lis[_index]
    

def mark_proband_file(family_info_file):
    flist = []
    with open(family_info_file, 'r') as fh:
        for line in fh.read().splitlines():
            if line.startswith('#'):
                continue
            
            data = line.split('\t')
            
            if len(data) < 2:           
                continue
            
            if not re.match(r'son|daughter|sibling|child|proband|brother|sister|half_brother|half_sister', data[1], re.I):
                continue
    
            cmd = 'find /gpfs0/prod_archive/res_clinical/ -name "*{}*.hard-filtered.vcf.gz"'.format(data[0].strip())
            p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = p.communicate()
            file_name = stdout.split()
            file_name = ''.join([f for f in file_name if 'BI' not in f])
            flist.append(file_name) 
    return flist


def main():    
    hpo_file_list = {f.split('_', 1)[0]: f for f in os.listdir('hpo-data') if not f.startswith('#') and f.endswith('.txt')}      
    
    res = []
    for k, v in hpo_file_list.items():
        with open('all_family_info.txt', 'r') as fh:
            for line in fh:
                if k in line:
                    res.append((int(k), v, line.strip()))
                    break
    
    from operator import itemgetter
    sorted_list = sorted(res, key=itemgetter(0))
    
    print('# family_info files: {}'.format(len(sorted_list)))
    for row in sorted_list:
        family_info_file = row[-1].strip()
        proband_file_list = mark_proband_file(family_info_file)
        
        for proband_file in proband_file_list:
            if proband_file:                
                print('{},{},{},{}'.format(row[0], row[1], family_info_file, proband_file))
    
if __name__ == '__main__':
    main()
