""" Update the yaml template file (if needed) and run Exomiser with 
input VCF file and HPO term(s). 
"""

import os
import sys
import yaml
from subprocess import check_call, call

template_file = 'template-analysis-exome.yml'

def main(_args):
    vcf_file = _args.vcf_file
    if vcf_file.endswith('.gz'):
        vcf_file = copy_and_unzip(vcf_file)
    print(vcf_file)    
    
    hpo_terms = filter(None, _args.hpo_terms.split(','))
    print(hpo_terms)    
    update_yml_template(vcf_file=vcf_file, hpo_terms=hpo_terms, output_prefix=_args.output_prefix)
    print('* yaml template - updated')
    
    # Run exomiser command line
    print('* Running Exomiser...')
    run_exomiser_cli()
    
    
def copy_and_unzip(f):
    temp_directory = './tests'
    gz_file = os.path.join(temp_directory, os.path.basename(f))
    call(['cp', f, temp_directory])
    call(['gunzip', gz_file])
    
    vcf_file = gz_file.replace('.gz', '')
    
    if os.path.isfile(gz_file):
      call(['rm', gz_file])
    return vcf_file


def update_yml_template(vcf_file=None, hpo_terms=None, output_prefix=None):
    with open(template_file, 'r') as ymlfile:
        template= yaml.load(ymlfile)

    template['analysis']['vcf'] = vcf_file
    template['analysis']['hpoIds'] = hpo_terms
    template['outputOptions']['outputPrefix'] = output_prefix
    
    with open(template_file, "w") as f:
        yaml.dump(template, f)
    

def run_exomiser_cli():
    exomiser_cli = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'exomiser-cli-10.1.0.jar') 
    cmd = ['java', '-Xms2g', '-Xmx4g', '-jar', exomiser_cli, '--analysis', template_file]
    print('>>>>>>>>>>>>>>>>>>>>>', cmd)
    call(cmd)


def is_valid_directory(path):
    if os.path.isdir(path):
        return True
    else:
        print("'{}' is not a directory.".format(path))
        sys.exit(1)


def is_valid_file(parser, arg):
    """Check if arg is a valid file that already exists on the file system."""
    arg = os.path.abspath(arg)
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return arg
    

def get_parser():
    """Gets parser object for the script"""

    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

    parser = ArgumentParser(description=__doc__,
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument('--vcf',
                        dest='vcf_file',
                        type=lambda x: is_valid_file(parser, x),
                        metavar='FILE',
                        help='VCF file of interest.',)

    parser.add_argument('--hpo',
                        dest='hpo_terms',
                        type=str,
                        help='HPO terms',)
  
    parser.add_argument('--output',
                        dest='output_prefix',
                        default='results_',
                        help="Specify output prefix or a fully qualifed path for the analysis results.")

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    main(args)
