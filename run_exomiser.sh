#!/bin/bash

PYTHON='/usr/bin/python'
PROBAND_INFO_FILE='/home/dtamang/projects/phenotype_mapping/tests/test_family_info.txt'

LOG_DIR='/home/dtamang/projects/phenotype_mapping/logs'
if [[ ! -e $LOG_DIR ]]; then
    mkdir $LOG_DIR
fi


# 302,./hpo-data/302...,/gpfs0/prod_archive/res_clinical/RCHSD/N2/302/CS3683-IN4C7C-R18AA345-D1L1/CS3683-IN4C7C-R18AA345-D1L1_dragen.vcf

#for file in ./202*.txt; do
    #family_name=$(echo $(basename ${file}) | cut -d'_' -f 1)
#    echo "------- ${file} -------"
#    tr '\n' ',' < ${file} > "${file}.csv"
#    hpo_terms=`cat "${file}.csv"`
#    echo $hpo_terms
    
    #while read line; do
    #    echo ">>> ${line}"  
    #done < ${hpo_terms}
#done

declare -A array # this is the only update
while read line; do
    [[ "$line" =~ ^#.*$ ]] && continue
    IFS=', ' read -r -a row <<< "$line"
    echo "------- ${row[1]} -------"
    tr '\n' ',' < "hpo-data/${row[1]}" > "${row[1]}.csv"
    hpo_terms=`cat "${row[1]}.csv"`
    python run_exomiser.py --vcf ${row[-1]} --hpo ${hpo_terms} --output "tests/output/${row[0]}"
#    srun -o "$LOG_DIR/${key}.log" -J "octopus" -S 3 -p all python runner.py ${key} ${array[$key]} &
    rm "${row[1]}.csv"
    
done < $PROBAND_INFO_FILE
    
    
    
#declare -A array # this is the only update
#while read line; do
#    [[ "$line" =~ ^#.*$ ]] && continue
#    if [[ ${line:0:3} == 'Fam' ]]
#      then
#        family=$line
#    else
#        IFS=', ' read -r -a row <<< "$line"
#        array[$family]="${array[$family]}${array[$family]:+;}$line"
#    fi
#done < $BAM_COVERAGE_FILE


#for key in "${!array[@]}"; do
#    echo "- ${key}" 
#    #python runner.py ${key} ${array[$key]}
#    srun -o "$LOG_DIR/${key}.log" -J "octopus" -S 3 -p all python runner.py ${key} ${array[$key]} &
#done